#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import computeoocount

calc = computeoocount.Compute()
ops = {
    'power': calc.power,
    'log': calc.log
}
