#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys

class Compute:
    """Esto es un ejemplo de clase que hereda de ClaseMadre"""

    def __init__(self, mensaje):
        """Esto es el método iniciliazador"""
        """Cada vez que se crea un nuevo objeto se ejecuta esta función init"""
        self.default = mensaje

    def power(self, n1, n2):
        return n1 ** n2

    def log(self, n1, n2):
        return math.log(n1, n2)



if __name__ == "__main__":

    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        n1 = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")
    try:
        n2 = float(sys.argv[3])
    except ValueError:
        sys.exit("Error: third argument should be a number")

    resultado = Compute(2)  # Creo un objeto que tiene como plantilla la clase Compute y le paso el dos por defecto

    if sys.argv[1] == "power":      # Si el primer valor que le pasas = power..
        print(resultado.power(n1, n2))
    elif sys.argv[1] == "log":      # Si el primer valor que le pasas = log..
        print(resultado.log(n1, n2))
    else:
        sys.exit('Operand should be power or log')
