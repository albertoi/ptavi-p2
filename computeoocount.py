#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys


class Compute:
    """Esto es un ejemplo de clase que hereda de ClaseMadre"""


    def __init__(self, mensaje=2):
        """Esto es el método iniciliazador"""
        """Cada vez que se crea un nuevo objeto se ejecuta esta función init"""
        self.default = mensaje
        self.count = 0


    def power(self, n1, n2):
        self.count = self.count + 1
        return n1 ** n2


    def log(self, n1, n2):
        self.count = self.count + 1
        return math.log(n1, n2)


    def set_def(self):
        self.count = self.count + 1
        if float(sys.argv[2]) <= 0:
            raise ValueError("ValueError")


    def get_def(self):
        self.count = self.count + 1
        return self.set_def()

