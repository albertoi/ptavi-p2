#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import computeoo


class ComputeChild(computeoo.Compute):

    def set_def(self):
        if int(sys.argv[2]) <= 0:
            raise ValueError("ValueError")

    def get_def(self):
       return self.set_def()



if __name__ == "__main__":

   if len(sys.argv) < 3:
       sys.exit("Error: at least two arguments are needed")

   try:
       n1 = float(sys.argv[2])
   except ValueError:
       sys.exit("Error: second argument should be a number")
   try:
       n2 = float(sys.argv[3])
   except ValueError:
       sys.exit("Error: third argument should be a number")
   if len(sys.argv) == 3:
       n2 = 2
   if len(sys.argv) > 3:
       n2 = float(sys.argv[3])
       try:
           n2 = float(sys.argv[3])
       except ValueError:
           sys.exit("Error")
   resultado = ComputeChild(2)  # Creo un objeto que tiene como plantilla la clase Computechld
   resultado.set_def()
   if sys.argv[1] == "power":      # Si el primer valor que le pasas = power..
       print(resultado.power(n1, n2))

   elif sys.argv[1] == "log":      # Si el primer valor que le pasas = log..
       print(resultado.log(n1, n2))



